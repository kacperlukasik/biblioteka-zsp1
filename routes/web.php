<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use \Illuminate\Support\Facades\URL;

Route::get('/', function () {
    return view('basic', []);
});


Auth::routes();

Route::get('/panel', 'HomeController@index')->name('panel');


Route::prefix('/panel')->group(function () {
    Route::get('reviews', 'HomeController@reviews')->name('panel.reviews');
    Route::delete('reviews/{id}/delete', 'HomeController@delete')->name('panel.reviews.delete');
    Route::get('reviews/{id}/edit', "HomeController@edit")->name('panel.reviews.edit');
    Route::put('reviews/{id}/store', "HomeController@store")->name('panel.reviews.store');
    Route::get('reviews/add', "HomeController@add")->name('panel.reviews.add');
    Route::post('reviews/insert', "HomeController@insert")->name('panel.reviews.insert');
    Route::post('books', "HomeController@search")->name('panel.books.search');
});