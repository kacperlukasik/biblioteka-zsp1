<?php

namespace App\Http\Controllers;

use App\Books;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Reviews;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('panel.panel');
    }

    public function reviews()
    {
        $reviews = Reviews::where("user_id", \Illuminate\Support\Facades\Auth::user()->id)->orderBy('updated_at', 'DESC')->paginate(5);
        return view('panel.reviews', ['reviews' => $reviews]);
    }

    public function delete($id)
    {
        if (Reviews::find($id)->user_id === Auth::user()->id) Reviews::find($id)->delete();
        return back();
    }

    public function edit(Request $request,$id)
    {
        if(Reviews::where("id",$id)->first()->accepted!=0)
        {
            $review = Reviews::find($id);
            return view('panel.edit', ['review' => $review]);
        }
        else
        {
            $request->session()->flash('emsg', "Nie można edytować niezaakceptowanej recenzji");
            return redirect()->route('panel.reviews');
        }

    }

    public function store(Request $request, $id)
    {
        Reviews::where('id', $id)->update([
            'title'=> $request->input('reviewTitle'),
            'content'=> $request->input('reviewContent'),
            'rating'=> $request->input('reviewRating'),
            'accepted'=>0
        ]);

        $request->session()->flash('msg', 'Zapisano pomyślnie i przesłano do akceptacji!');

        return redirect()->route('panel.reviews');
    }

    public function add()
    {
        return view('panel.add');
    }

    public function insert(Request $request)
    {
        $review = new \App\Reviews();
        $review->fill([
            "title"=> $request->input('reviewTitle'),
            "content"=>$request->input('reviewContent'),
            "link"=>"link",
            "accepted"=>0,
            "rating"=>$request->input('reviewRating'),
            "user_id"=>Auth::user()->id,
            "book_id"=>$request->input('reviewBook'),
            "category_id"=>1
        ]);
        $review->save();
        $request->session()->flash('msg', "Recenzje dodano do akceptacji");
        return redirect()->route('panel.reviews');
    }

    public function search(Request $request)
    {
        $search = $request->input('search');

        return Books::where('title', "LIKE", "%$search%")->take(10)->get();
    }
}
