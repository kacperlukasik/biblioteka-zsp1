<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    protected $fillable = [
        'title', 'author', 'cover', 'category_id',
    ];

    public function category()
    {
        return $this->hasMany("App\Category", "id", "category_id");
    }

    public $timestamps = false;
}
