<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public function author()
    {
        return $this->hasOne("App\User", "id", "user_id");
    }

    public function categories()
    {
        return $this->hasMany("App\NewsCategory", "id", "category_id");
    }
}
