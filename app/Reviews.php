<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{
    protected $fillable = ['title', 'content', 'link', "accepted", "rating", "user_id", "book_id", "category_id"];

    public function author()
    {
        return $this->hasOne("App\User", "id", "user_id");
    }

    public function book()
    {
        return $this->hasOne("App\Books", "id", "book_id");
    }

    public function categories()
    {
        return $this->hasMany("App\ReviewCategory", "id", "category_id");
    }
}
