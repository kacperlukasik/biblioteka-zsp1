<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewCategory extends Model
{
    protected $fillable = [
        'name'
    ];

    public $timestamps = false;
}
