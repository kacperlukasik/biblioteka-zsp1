<?php

namespace App;

use App\Notifications\ResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    public function roles()
    {
        $this->belongsToMany(Roles::class,"user_has_roles","user_id","roles_id")->withTimestamps();
    }

    public function  hasRoles($roles)
    {
        if (is_array($roles))
        {
            foreach ($roles as $role)
            {
                if ($this->CheckRole($role))
                    return true;
            }
        }
        else
        {
            return $this->CheckRole($roles);
        }

        return false;
    }

    private function CheckRole($role)
    {
        if ($this->roles()->where('name', $role)->first())
            return true;
        return false;
    }
}
