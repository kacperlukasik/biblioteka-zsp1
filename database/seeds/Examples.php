<?php

use Illuminate\Database\Seeder;

class Examples extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exCount = 10;

        for ($i = 1; $i<=$exCount;$i++)
        {
            $user = new \App\User();
            $user->fill([
                "name"=>"User #".$i,
                "email"=>"example".$i."@example.pl",
                "password"=>bcrypt("example")
            ]);
            $user->save();
        }

        for ($i = 1; $i<=$exCount;$i++)
        {
            $category = new \App\Category();
            $category->fill([
                "name"=>"Category #".$i
            ]);
            $category->save();
        }

        for ($i = 1; $i<=$exCount;$i++)
        {
            $category = new \App\ReviewCategory();
            $category->fill([
                "name"=>"Review Category #".$i
            ]);
            $category->save();
        }

        for ($i = 1; $i<=$exCount;$i++)
        {
            $category = new \App\NewsCategory();
            $category->fill([
                "name"=>"News Category #".$i
            ]);
            $category->save();
        }

        for ($i = 1; $i<=$exCount;$i++)
        {
            $book = new \App\Books();
            $book->fill([
                "title"=>"Title #".$i,
                "author"=>"Author #".$i,
                "cover"=>"example".$i.".png",
                "category_id"=>rand(1,$exCount)
            ]);
            $book->save();
        }

        for ($i = 1; $i<=$exCount;$i++)
        {
            $review = new \App\Reviews();
            $review->fill([
                "title"=>"Title #".$i,
                "content"=>"Content #".$i,
                "link"=>"link".$i,
                "accepted"=>1,
                "rating"=>rand(0,10),
                "user_id"=>rand(1,$exCount),
                "book_id"=>rand(1, $exCount),
                "category_id"=>rand(1,$exCount)
            ]);
            $review->save();
        }

        for ($i = 1; $i<=$exCount;$i++)
        {
            $news = new \App\News();
            $news->fill([
                "title"=>"Title #".$i,
                "content"=>"Content #".$i,
                "link"=>"link".$i,
                "pinned"=>rand(0,1),
                "user_id"=>rand(1,$exCount),
                "category_id"=>rand(1,$exCount)
            ]);
            $news->save();
        }

        for ($i = 1; $i<=$exCount;$i++)
        {
            $comment = new \App\Comments();
            $comment->fill([
                "content"=>"Content #".$i,
                "accepted"=>rand(0,1),
                "user_id"=>rand(1,$exCount),
                "review_id"=>rand(1,$exCount)
            ]);
            $comment->save();
        }
    }
}
