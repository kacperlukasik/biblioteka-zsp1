@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
        </div>
        <div class="row justify-content-center" style="margin-bottom: 10px;">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <span style="font-weight: bold">Dodaj recenzje</span>
                    </div>

                    <div class="card-body" id="app">
                        <form action="{{ route("panel.reviews.insert") }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="reviewTitle">Tytuł</label>
                                <input type="text" id="reviewTitle" name="reviewTitle" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="reviewContent">Treść</label>
                                <textarea id="reviewContent" class="form-control" name="reviewContent" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="reviewRating">Ocena</label>
                                <select name="reviewRating" id="reviewRating" class="form-control" required>
                                    @for($i=1;$i<=10;$i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="reviewBookSelect">Książka</label>
                                <input v-model="queryString" class="form-control" type="text" id="bookSearch">
                                <select class="form-control" name="reviewBook" id="reviewBookSelect" required>
                                    <option value="" v-for="option in optionsArray" v-bind:value="option.id">@{{ option.title }}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success">Dodaj</button>
                                <a href="{{route('panel.reviews')}}">Powrót do recenzji</a>
                            </div>
                            @if(Session::has('save'))
                                <div class="alert alert-success" role="alert">
                                    {{ Session::get('save') }}
                                </div>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{--<script>
        $("#btnShow").click(function(evt){ajaxCallback($('#bookSearch').val())});var ajaxCallback=function(val){$("#reviewBookSelect").html();$.ajaxSetup({headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')}});$.ajax({type:'POST',url:"{{route('panel.books.search')}}",data:{search:val},dataType:'json',success:function(data){var tmp="";$.each(data,function(index,element){tmp+="<option value='"+element.id+"'>"+element.title+"</option>"});$('#reviewBookSelect').html(tmp)}})}
    </script>--}}

    <script>
        var token = document.querySelector("meta[name='csrf-token']").getAttribute('content');

        const HTTP = axios.create({
            baseURL: '{{route('panel.books.search')}}',
            headers: {
                'X-CSRF-TOKEN': token
            }
        });

        var vm = new Vue({
            el: "#app",
            data: {
                queryString: "",
                optionsArray: null,
            },
            methods:{
                getQueryData: function(qString){
                    var vm = this;
                    HTTP.post('',{search:qString}).then(function (response) {
                        vm.optionsArray = response.data;
                    });
                }
            },
            watch: {
                queryString: _.debounce(function (data) {
                    this.getQueryData(data);
                }, 500)
            },
        });
        vm.getQueryData();
    </script>
@endsection
