@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if($reviews->count())
            <h2>Twoje recenzje</h2>
        @else
            <h2>Nie posiadasz recenzji</h2>
        @endif
    </div>
    <div class="row justify-content-center">
        <a href="{{route('panel.reviews.add')}}">Dodaj nowa</a>
    </div>
    @if(Session::has('msg'))
        <div class="alert alert-success" role="alert">
            {{ Session::get('msg') }}
        </div>
    @endif
    @if(Session::has('emsg'))
        <div class="alert alert-danger" role="alert">
            {{ Session::get('emsg') }}
        </div>
    @endif
    @foreach($reviews as $review)
        <div class="row justify-content-center" style="margin-bottom: 10px;">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        {{ $review->title }}
                        @if(!$review->accepted)
                            - niezaakceptowana !
                        @endif
                    </div>

                    <div class="card-body">
                        {{ str_limit($review->content, 800, ' (...)') }}
                    </div>

                    <div class="card-header">
                        @if($review->accepted)
                            <form class="d-inline" action="{{ route('panel.reviews.edit', $review->id) }}" method="GET">
                                {{ csrf_field() }}
                                <button class="btn btn-warning">Edytuj</button>
                            </form>
                        @endif
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#saveModal{{$review->id}}">
                            Usuń
                        </button>
                        <span>Edytowano: {{ $review->updated_at }}</span>
                    </div>
                </div>
            </div>
        </div>
        <!--SAVE MODAL-->
        <div id="saveModal{{$review->id}}" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Na pewno ?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Czy napewno chesz usunąć tą recenzje ?</p>
                    </div>
                    <div class="modal-footer">
                        <form class="d-inline" action="{{ route('panel.reviews.delete' , $review->id ) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-primary">Tak</button>
                        </form>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Nie</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <div class="row justify-content-center">
        {{ $reviews->links() }}
    </div>
</div>
@endsection
