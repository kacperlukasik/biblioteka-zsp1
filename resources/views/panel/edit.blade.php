@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
    </div>
        <div class="row justify-content-center" style="margin-bottom: 10px;">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <span style="font-weight: bold">Edytuj</span>
                    </div>

                    <div class="card-body">
                        <form action="{{ route("panel.reviews.store", $review->id) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="form-group">
                                <label for="reviewTitle">Tytuł</label>
                                <input type="text" id="reviewTitle" name="reviewTitle" class="form-control" value="{{ $review->title }}">
                            </div>
                            <div class="form-group">
                                <label for="reviewContent">Treść</label>
                                <textarea id="reviewContent" class="form-control" name="reviewContent">{{ $review->content }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="reviewRating">Ocena</label>
                                <select name="reviewRating" id="reviewRating" class="form-control">
                                    @for($i=1;$i<=10;$i++)
                                        @if($review->rating==$i)
                                            <option value="{{$i}}" selected>{{$i}}</option>
                                        @else
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endif
                                    @endfor
                                </select>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success">Zapisz</button>
                                <a href="{{route('panel.reviews')}}">Powrót</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
